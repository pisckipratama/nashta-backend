# Nashta-Backend

#### for running in your local development
- npm install
- set .env
- npm start

#### Tech Stack
PostgreSQL, ExpressJS, NodeJS, JWT Auth.

#### Link Public
- [Heroku](https://nashta-api.herokuapp.com)
- [Postman Colletion](https://www.getpostman.com/collections/0a4587ed2ba8238c8fc2)

#### Todo
- [ ] Create Unit Testing
- [ ] Create Caching with Redis
- [ ] Hashing password
- [ ] Dockerize App