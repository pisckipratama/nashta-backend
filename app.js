const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const dotenv = require('dotenv');
const cors = require('cors');

dotenv.config();
const { pool } = require('./config/db');

const indexRouter = require('./routes/index')(pool);
const usersRouter = require('./routes/users')(pool);

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/api/v1/mahasiswa', indexRouter);
app.use('/api/v1/users', usersRouter);

module.exports = app;
