CREATE TABLE mahasiswa(
	id SERIAL PRIMARY KEY,
	nama VARCHAR(30),
	alamat TEXT
);

CREATE TABLE matakuliah(
	id SERIAL PRIMARY KEY,
	namamatkul VARCHAR(50),
	idmhs INT,
	FOREIGN KEY (idmhs) REFERENCES mahasiswa(id)
);

CREATE TABLE nilai(
	id SERIAL PRIMARY KEY,
	idmhs INT,
	idmatkul INT,
	nilai SERIAL,
	keterangan TEXT,
	FOREIGN KEY (idmhs) REFERENCES mahasiswa(id),
	FOREIGN KEY (idmatkul) REFERENCES matakuliah(id)
);

CREATE TABLE users(
  id SERIAL PRIMARY KEY,
  username VARCHAR(16),
  password VARCHAR(32)
);

INSERT INTO users(username, password) VALUES('admin', '12345'), ('tamu', '12345');

INSERT INTO mahasiswa(nama, alamat) 
VALUES
('Aldo', 'Subang'), 
('Budi', 'Cianjur'),
('Candra', 'Sumedang'),
('Dewi', 'Tasikmalaya'),
('Elisa', 'Garut');

INSERT INTO matakuliah(namamatkul, idmhs) 
VALUES
('Matematika Diskrit', 1),
('Pemrograman Web', 2),
('Pemrograman Dasar', 3),
('Jaringan Komputer', 4),
('Aljabar Linear', 5);