const { Pool } = require('pg');

const pool = new Pool({
  user: process.env.POSTGRES_USER || 'postgres',
  host: process.env.POSTGRES_HOST || 'localhost',
  database: process.env.POSTGRES_DB || 'pgapi',
  password: process.env.POSTGRES_SECRET,
  port: process.env.POSTGRES_PORT || 5432,
});

module.exports = { pool };