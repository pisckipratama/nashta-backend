const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

module.exports = (pool) => {
  /** POST Login user */
  router.post('/', async (req, res) => {
    const { username, password } = req.body;
    try {
      const user = await pool.query(`SELECT * FROM users WHERE username=$1`, [username]);
      const data = user.rows[0];
      if (!data) return res.status(401).json({ "status": "error", "message": "login error!" });
      if (data.password !== password) return res.status(401).json({ "status": "error", "message": "login error!" });

      const payload = { user: { id: data.id, username: data.username } };
      jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 3600 }, (err, token) => {
        if (err) throw err;
        res.json({ msg: "login success", user: payload.user, token });
      });
    } catch (error) {
      console.error(error);
      res.json(error.msg);
    }
  });

  return router;
};
