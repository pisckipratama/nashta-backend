const express = require('express');
const { route } = require('./users');
const router = express.Router();
const jwtAuth = require('../middleware/jwtAuth');

module.exports = (pool) => {

  /* GET data mahasiswa. */
  router.get('/', jwtAuth, async (req, res) => {
    const query = `SELECT mahasiswa.id AS idmahasiswa, mahasiswa.nama, matakuliah.namamatkul, nilai.nilai, nilai.keterangan FROM nilai LEFT JOIN mahasiswa ON nilai.idmhs = mahasiswa.id LEFT JOIN matakuliah ON nilai.idmatkul = matakuliah.id`;

    try {
      const dataMahasiswa = await pool.query(query);
      res.status(200).json({ success: true, msg: "success retrive data", data: dataMahasiswa.rows });
    } catch (error) {
      console.error(500);
      res.status(500).json({ success: false, msg: error.msg });
    }
  });

  /* GET data nilai rata-rata masing-masing mahasiswa. */
  router.get('/averagescore', jwtAuth, async (req, res) => {
    const query = 'SELECT mahasiswa.id AS idmahasiswa, mahasiswa.nama, nilai.ratarata FROM (SELECT idmhs, AVG(nilai) as ratarata FROM nilai GROUP BY idmhs) AS nilai LEFT JOIN mahasiswa ON nilai.idmhs = mahasiswa.id ORDER BY mahasiswa.nama';

    try {
      const dataNilai = await pool.query(query);
      const dataFinal = dataNilai.rows.map(item => {
        item.ratarata = item.ratarata.toFixed(2);
        return item;
      });
      res.status(200).json({ success: true, msg: "success retrive data", data: dataFinal });
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, msg: error.msg });
    }
  });

  /* POST nilai */
  router.post('/', jwtAuth, async (req, res) => {
    const { nama, matakuliah, nilai, keterangan } = req.body;
    let idMahasiswa;
    let idMatkul;

    try {
      const getMhs = await pool.query("SELECT * FROM mahasiswa WHERE nama=$1", [nama]);
      const dataMhs = getMhs.rows[0];

      if (dataMhs) {
        idMahasiswa = dataMhs.id;
      } else {
        return res.status(400).json({ success: false, msg: "mahasiswa not found!" });
      }

      const getMatkul = await pool.query("SELECT * FROM matakuliah WHERE namamatkul=$1", [matakuliah]);
      const dataMatkul = getMatkul.rows[0];

      if (dataMatkul) {
        idMatkul = dataMatkul.id;
      } else {
        return res.status(400).json({ success: false, msg: "matakuliah not found!" });
      }

      const getNilai = await pool.query("SELECT * FROM nilai WHERE idmhs=$1 AND idmatkul=$2", [parseInt(idMahasiswa), parseInt(idMatkul)]);
      const dataNilai = getNilai.rows[0];
      if (dataNilai) return res.status(400).json({ success: false, msg: "data already exist! " });

      let queryCreateNilai = `INSERT INTO nilai(idmhs, idmatkul, nilai, keterangan) VALUES($1, $2, $3, $4)`;
      await pool.query(queryCreateNilai, [parseInt(idMahasiswa), parseInt(idMatkul), parseFloat(nilai), keterangan]);

      res.status(201).json({ success: true, msg: "add nilai success!" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, "msg": error.message });
    }
  })

  /** PUT Edit nilai mahasiswa */
  router.put('/:id', jwtAuth, async (req, res) => {
    const { nama, matakuliah, nilai, keterangan } = req.body;
    let idMahasiswa;
    let idMatkul;

    try {
      const getMhs = await pool.query("SELECT * FROM mahasiswa WHERE nama=$1", [nama]);
      const dataMhs = getMhs.rows[0];

      if (dataMhs) {
        idMahasiswa = dataMhs.id;
      } else {
        return res.status(400).json({ success: false, msg: "mahasiswa not found!" });
      }

      const getMatkul = await pool.query("SELECT * FROM matakuliah WHERE namamatkul=$1", [matakuliah]);
      const dataMatkul = getMatkul.rows[0];

      if (dataMatkul) {
        idMatkul = dataMatkul.id;
      } else {
        return res.status(400).json({ success: false, msg: "matakuliah not found!" });
      }

      const getNilai = await pool.query("SELECT * FROM nilai WHERE idmhs=$1 AND idmatkul=$2 AND id=$3", [parseInt(idMahasiswa), parseInt(idMatkul), parseInt(req.params.id)]);
      const dataNilai = getNilai.rows[0];
      if (!dataNilai) return res.status(400).json({ success: false, msg: "data nilai not found! " });

      let queryEditNilai = "UPDATE nilai SET idmhs=$1, idmatkul=$2, nilai=$3, keterangan=$4 WHERE id=$5"
      await pool.query(queryEditNilai, [parseInt(idMahasiswa), parseInt(idMatkul), parseFloat(nilai), keterangan, parseInt(dataNilai.id)]);

      res.status(201).json({ success: true, msg: "edit nilai success!" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, "msg": error.message });
    }
  });

  /** DELETE delete nilai mahasiswa */
  router.delete('/:id', jwtAuth, async (req, res) => {
    try {
      const getNilai = await pool.query("SELECT * FROM nilai WHERE id=$1", [parseInt(req.params.id)]);
      if (getNilai.rows.length === 0) return res.status(400).json({ success: false, msg: "data nilai not found! " });

      const deleteQuery = "DELETE FROM nilai WHERE id=$1";
      await pool.query(deleteQuery, [parseInt(req.params.id)]);
      res.status(201).json({ success: true, msg: "delete nilai success!" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, "msg": error.message });
    }
  });

  /* GET All nilai */
  router.get('/nilai', jwtAuth, async (req, res) => {
    const query = `SELECT nilai.id, mahasiswa.nama, matakuliah.namamatkul, nilai.nilai, nilai.keterangan FROM nilai LEFT JOIN mahasiswa ON nilai.idmhs = mahasiswa.id LEFT JOIN matakuliah ON nilai.idmatkul = matakuliah.id`;

    try {
      const dataNilai = await pool.query(query);
      res.status(200).json({ success: true, msg: "success retrive data", data: dataNilai.rows });
    } catch (error) {
      console.error(500);
      res.status(500).json({ success: false, msg: error.message });
    }
  });

  return router;
};