const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
  const token = req.header("Authorization");
  // res.status(401).send("Unauthorize!");
  if (!token) return res.status(401).json({ message: "You must be logged in!" });

  try {
    const decode = jwt.verify(token, process.env.JWT_SECRET);
    req.user = decode.user;
    next();
  } catch (err) {
    console.error(err.message);
    // res.status(500).json({ message: err.message });
    return res.status(500).json({ message: err.message });
  }
};